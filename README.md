# Scops
`scops`, your friendly neighborhood ping sweeper

## Installation
1. `git clone https://gitlab.com/krakjn/scops.git`
1. `cd scops`
1. `python3 -m venv .venv/` 
1. `source .venv/bin/activate` 
1. `pip install --editable .`  

> This will install `scops` in the newly created virtual environment

## Features
Running `scops --help` shows the following
```
Usage: scops [OPTIONS] IP_ADDRS

  Scope out outliers on each subnet, only IPv4 is supported
  any host that doesn't appear on all subnets will be listed
  CIDR notation is not supported as '/24' is implied with every subnet

  example:  scops -v 192.168.10.0 192.168.20.0

Options:
  -v, --verbose          show all active hosts
  -r, --retries INTEGER  number of ping retries
  -w, --wait INTEGER     seconds to wait for reply
  -s, --skip OCTECT      host to skip across subnets
  --help                 Show this message and exit.  
```

`scops` is **multithreaded!** On a 6-Core Intel Core i5, 3.3GHz a single subnet scan is under 2 seconds. `scops` will print out the time it took to scan all subnets. 

### Example 
`scops` will scan `1+` networks for outliers and print them out  
Given the this subnet layout with their respective active hosts:  
```json
subnets = {
    "172.10.20": [
        "172.10.20.23",
        "172.10.20.100",
        "172.10.20.55",
        "172.10.20.47",
    ],
    "172.10.22": [
        "172.10.22.11",
        "172.10.22.100",
        "172.10.22.55",
        "172.10.22.47",
        "172.10.22.41",
        "172.10.22.22",
        "172.10.22.8",
    ],
    "172.10.45": [
        "172.10.45.145",
        "172.10.45.100",
        "172.10.45.55",
        "172.10.45.47",
        "172.10.45.42",
        "172.10.45.22",
        "172.10.45.8",
    ]
}
```
The print out will be:
```
--outliers--
----172.10.22.11        
----172.10.20.23        
----172.10.22.41        
----172.10.45.42        
----172.10.45.145 
```
