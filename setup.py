import setuptools

setuptools.setup(
    name="scops",
    version="0.1.0",
    packages=["src"],
    install_requires=["click"],
    python_version=">=3.6",
    entry_points={"console_scripts": ["scops = src.cli:entry"]}
)
