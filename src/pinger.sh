#!/bin/bash
# Scops eyes on the field
SUBNET=$1
WAIT=$2
RETRIES=$3
THREAD_CALL=$4

function scan {
    ping "${SUBNET}.${THREAD_CALL}" -c 1 ${WAIT} > /dev/null && echo "${SUBNET}.${THREAD_CALL}" &
}

for i in {1..${RETRIES}}; do scan && break || sleep 2; done
