"""Unittest for scops."""
from src import scops
from src import utils

SUBNETS = ["192.168.45.0", "192.168.48.0"]


def test_subnet_truncation() -> None:
    """Ensure last octect is being stripped."""
    assert "192.168.45" == utils.strip_last_octet(SUBNETS[0])


def test_check_ip() -> None:
    """Ensure check_ip functions properly."""
    assert utils.check_ip("0.1.2.3") is False
    assert utils.check_ip("1.2.256.0") is False
    assert utils.check_ip("2.3.-3.4") is False
    assert utils.check_ip("123.23.34.56") is True
    assert utils.check_ip("123.23.") is False
    assert utils.check_ip("123.23.1.15.1") is False


def test_subnet_dict():
    """Ensure creation of subnet_dict has appropriate number keys."""
    scoper = scops.Scops([], SUBNETS)
    d = scoper.create_subnet_dict(SUBNETS)
    print("subnet dict: ", d)
    assert len(d.keys()) == 2


def test_ping() -> None:
    """Ensure ping picks up active hosts on a known good subnet."""
    scoper = scops.Scops((), SUBNETS)
    scoper.sweep()
    keys = list(scoper.subnets_dict.keys())
    assert len(scoper.subnets_dict[keys[0]]) > 1


def test_find_outliers():
    """Ensure only unique outliers are captured."""
    subnets = {
        "172.10.20": [
            "172.10.20.23",  # unique
            "172.10.20.100",
            "172.10.20.55",
            "172.10.20.47",
        ],
        "172.10.22": [
            "172.10.22.11",  # unique
            "172.10.22.100",
            "172.10.22.55",
            "172.10.22.47",
            "172.10.22.41",  # unique
            "172.10.22.22",
            "172.10.22.8",
        ],
        "172.10.45": [
            "172.10.45.145",  # unique
            "172.10.45.100",
            "172.10.45.55",
            "172.10.45.47",
            "172.10.45.42",  # unique
            "172.10.45.22",
            "172.10.45.8",
        ],
    }
    outliers = utils.get_outliers(subnets)
    assert len(outliers) == 5
