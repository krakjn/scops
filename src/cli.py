"""Command Line Interface for scops."""
import click
import time
from typing import List, Tuple
from src import scops


@click.command(name="scops")
@click.option("-v", "--verbose", is_flag=True, help="show all active hosts")
@click.option("-r", "--retries", default=1, help="number of ping retries")
@click.option("-w", "--wait", default=2, help="seconds to wait for reply")
@click.option(
    "-s",
    "--skip",
    metavar="OCTECT",
    multiple=True,
    type=click.INT,
    default=[],
    help="octet to skip across subnets",
)
@click.argument("ip_addrs", metavar="IP_ADDRS", nargs=-1)
def entry(
    verbose: bool,
    retries: int,
    wait: int,
    skip: Tuple[str],
    ip_addrs: List[str],
):
    """
    \b
    Scope out outliers on each subnet, only IPv4 is supported
    any host that doesn't appear on all subnets will be listed
    CIDR notation is not supported as '/24' is implied with every subnet

    example:  scops -v 192.168.10.0 192.168.20.0
    """
    if len(ip_addrs) == 0:
        print("no IP_ADDR supplied, aborting...")
        exit(-2)
    begin = time.time()

    scoper = scops.Scops(skip, ip_addrs, wait=wait, retries=retries)
    scoper.sweep()
    if verbose:
        scoper.show_reachable()
    scoper.show_outliers()

    elapse = time.time() - begin
    print(f"ping sweep done in: \x1b[1;32m{elapse:0.3f}s\x1b[0m")
