"""Utility functions for scops."""
import re
import platform
from functools import cmp_to_key

IP_REGEX = re.compile(r"^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$")


def strip_last_octet(subnet) -> str:
    """Return subnet stripped of last octet."""
    matched = re.match(IP_REGEX, subnet)
    if matched:
        return f"{matched.group(1)}.{matched.group(2)}.{matched.group(3)}"


def get_last_octet(ip_addr: str) -> str:
    """Return last octect of given ip_addr."""
    matched = re.match(IP_REGEX, ip_addr)
    if matched:
        return matched.group(4)


def check_ip(ip_addr) -> bool:
    """Determine if ip_addr is proper IPv4 address."""
    retval = True
    matched = re.match(IP_REGEX, ip_addr)
    if matched:
        if int(matched.group(1)) == 0:
            print(f"{matched.group(1)} is not allowed as first octect")
            retval = False
        for octet_str in matched.groups():
            octet = int(octet_str)
            if octet < 0 or octet > 255:
                print(f"octet {octet_str} needs to be within 0-255")
                retval = False
    else:
        retval = False
    return retval


def sort_ips(ip_addr_a: str, ip_addr_b: str) -> int:
    """Sort for ip_addrs in ascending order."""
    a_matched = re.match(IP_REGEX, ip_addr_a)
    b_matched = re.match(IP_REGEX, ip_addr_b)
    if int(a_matched.group(1)) < int(b_matched.group(1)):
        return -1
    if int(a_matched.group(2)) < int(b_matched.group(2)):
        return -1
    if int(a_matched.group(3)) < int(b_matched.group(3)):
        return -1
    if int(a_matched.group(4)) < int(b_matched.group(4)):
        return -1
    if int(a_matched.group(1)) > int(b_matched.group(1)):
        return 1
    if int(a_matched.group(2)) > int(b_matched.group(2)):
        return 1
    if int(a_matched.group(3)) > int(b_matched.group(3)):
        return 1
    if int(a_matched.group(4)) > int(b_matched.group(4)):
        return 1
    return 0


def get_wait_flag(wait: int) -> str:
    """Return correct wait/deadline ping flag per system."""
    # system/OS name, such as 'Linux', 'Darwin', 'Java', 'Windows'
    if platform.system() == "Darwin":
        return f"-W {wait}"
    else:
        return f"-w {wait}"


def get_outliers(subnets: dict) -> list:
    """
    Sift through subnets determining outliers that are unique across subnets.

    subnets:    dictionary of subnets
                key:    relative subnet
                value:  list of active hosts
    """
    keys = list(subnets.keys())
    outliers = []
    if len(keys) >= 1:
        # start with copy to argue against
        outliers = subnets[keys[0]]
        popped_octets = []  # to capture repeats

        # to iterate from second subnet onward
        for k in range(1, len(keys)):
            for hit in subnets[keys[k]]:
                popped = False
                for idx, val in enumerate(outliers):
                    hit_octet = get_last_octet(hit)
                    val_octet = get_last_octet(val)
                    if hit_octet == val_octet:
                        outliers.pop(idx)
                        popped_octets.append(hit_octet)
                        popped = True
                    elif hit_octet in popped_octets:
                        popped = True
                if not popped:
                    # outlier found
                    outliers.append(hit)

    return outliers


def print_outliers(outliers: list) -> None:
    """Nice console output of outliers."""
    if len(outliers) >= 1:
        print(f"\x1b[0;33m--outliers--\x1b[0m")
        outliers.sort(key=cmp_to_key(sort_ips))
        for outlier in outliers:
            print(f"----{outlier:<20}")
    else:
        print(f"\x1b[0;31mno outliers found\x1b[0m")
