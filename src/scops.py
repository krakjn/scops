"""Scops is a active host lookup utility based on given subnets."""
#    ____
#   / __/______  ___  ___
#  _\ \/ __/ _ \/ _ \(_-<
# /___/\__/\___/ .__/___/
#             /_/
import subprocess
from typing import List, Iterator
from concurrent.futures import ThreadPoolExecutor
from functools import cmp_to_key
from src import utils


class Scops:
    """class that scopes out the subnets."""

    def __init__(
        cls, skip_vals: List[int], subnets: List[str], wait=2, retries=1
    ):
        """
        Scops constructor, using cls modifier to showcase that only one
        instance of Scops is needed.

        skip_vals:  list of octets to skip across subnets
        subnets:    list of ip_addrs to scan
        wait:       time in secs for ping to wait for reply
        retires:    number of retries for a failed ping
        return:     Scops class instance
        """
        cls.skip_vals: list = list(skip_vals)
        cls.octet: list = [i for i in range(1, 254) if i not in skip_vals]
        cls.subnets_dict: dict = cls.create_subnet_dict(subnets)
        cls.subnets_iter: Iterator = iter(cls.subnets_dict.keys())
        cls.current_subnet: str = ""
        # subprocess flags need to be in str format
        cls.wait: str = utils.get_wait_flag(wait)
        cls.retries: str = str(retries)

    def create_subnet_dict(cls, subnets: List[str]) -> dict:
        """
        Error checks and creates subnets_dict with the last octet stripped.

        subnets: list of subnets to strip and load into scops
        return:  dictionary of subnets with the key being the subnet
                 they represent
        """
        _dict = {}
        for subnet in subnets:
            if utils.check_ip(subnet):
                _dict[utils.strip_last_octet(subnet)] = []
            else:
                print("scops supports a netmask of 255.255.255.0")
                print(f"invalid subnet '{subnet}', aborting")
                exit(-1)
        return _dict

    def ping(cls, i) -> None:
        """
        Call out to a shell script to fork off in the background
        If the ping is successful appends to active list in current subnet.

        i:  octet provided from threaded call
        """
        proc = subprocess.run(
            [
                "sh",
                "src/pinger.sh",
                cls.current_subnet,
                cls.wait,
                cls.retries,
                str(i),
            ],
            capture_output=True,
        )
        if proc.stdout:
            cls.subnets_dict[cls.current_subnet].append(
                proc.stdout.decode("utf8").strip("\n")
            )

    def next_subnet(cls) -> None:
        """Generator to set next subnet."""
        for subnet in cls.subnets_iter:
            cls.current_subnet = subnet
            yield

    def sweep(cls) -> None:
        """Scan current subnet with 254 threads."""
        for _ in cls.next_subnet():
            print(
                f"scanning subnet...  {cls.current_subnet:>9}.[0-255]\t",
                end="",
            )
            print(
                f"skipping: {cls.skip_vals}" if len(cls.skip_vals) >= 1 else ""
            )
            with ThreadPoolExecutor(max_workers=254) as pinger:
                pinger.map(cls.ping, cls.octet)

            # use custom sorter to sort ip_addrs
            cls.subnets_dict.get(cls.current_subnet).sort(
                key=cmp_to_key(utils.sort_ips)
            )

    def show_reachable(cls) -> None:
        """For verbose printout, show all reachable hosts."""
        for subnet, actives in cls.subnets_dict.items():
            print(f"subnet: {subnet}\n  reachable:")
            for active in actives:
                print(f"\t{active}")

    def show_outliers(cls) -> None:
        """Filter out active hosts across all subnets."""
        outliers = utils.get_outliers(cls.subnets_dict)
        utils.print_outliers(outliers)
